![screenshot]

Other Screenshots: [02]  [03]  [04]  [05]

# [COMPONENTES]

[![minetest_icon]][minetest_link] Adds various electronic and mechanical components that will serve as crafting ingredients for various mods.


## 📦 **Dependencies:**

| Mod Name | Dependency Type | Descryption |
| :--: | :--: | :-- |
| default | Mandatory |  [Minetest Game] Included. | 
| dye | Mandatory | [Minetest Game] Included. |
| flowers | Mandatory | [Minetest Game] Included. |
| keys | Mandatory | [Minetest Game] Included. |
| wool | Mandatory | [Minetest Game] Included. |
| [Unified Inventory] | Optional | Replaces the default inventory and adds a number of features, such as a crafting guide. |
| [Head Lamp] | Optional | Adds a Head Lamp that can be charged by a battery from mod components. |


## **License:**

* [![license_icon]][license_link]

See more details in wiki: 
 
[![wiki_en_icon]][wiki_en_link] [![wiki_pt_icon]][wiki_pt_link]

## **Developers:**

* Lunovox Heavenfinder: [email](mailto:lunovox@disroot.org), [social web](https://qoto.org/@lunovox), [WebChat](https://cloud.disroot.org/call/9aa2t7ib), [xmpp](xmpp:lunovox@disroot.org?join), [audio conference](mumble:mumble.disroot.org), [more contacts](https:libreplanet.org/wiki/User:Lunovox)

## **Downloads:**

* [Stable Versions] : There are several versions in different stages of completion.  Usually recommended for servers because they are more secure.

* [Git Repository] : It is the version currently under construction.  There may be some stability if used on servers.  It is only recommended for testers and mod developers.

## **Inernacionalization:**

### **Available Languages:**

* English [Defaul, Concluded: 100%]
* Português (Concluded: 0%)

### **Translate this mod to your Language:**

See more details in file: [locale/README.md]


[screenshot]:https://gitlab.com/lunovox/components/-/raw/main/screenshot.png
[02]:https://gitlab.com/lunovox/components/-/raw/main/screenshot.2.png
[03]:https://gitlab.com/lunovox/components/-/raw/main/screenshot.3.png
[04]:https://gitlab.com/lunovox/components/-/raw/main/screenshot.4.png
[05]:https://gitlab.com/lunovox/components/-/raw/main/screenshot.5.png
[correio]:https://gitlab.com/lunovox/correio
[Git Repository]:https://gitlab.com/lunovox/components
[Head Lamp]:https://content.minetest.net/packages/OgelGames/headlamp/
[intllib]:https://github.com/minetest-mods/intllib
[license_icon]:https://img.shields.io/static/v1?label=GNU%20AGPL%20v3.0&message=Download&color=yellow
[license_link]:https://gitlab.com/lunovox/components/-/raw/main/LICENSE
[locale/README.md]:https://gitlab.com/lunovox/components/-/tree/main/locale?ref_type=heads
[COMPONENTES]:https://gitlab.com/lunovox/components
[minetest_icon]:https://img.shields.io/static/v1?label=Minetest&message=Mod&color=brightgreen
[minetest_link]:https://minetest.net
[Minetest Game]:https://content.minetest.net/packages/Minetest/minetest_game/
[Stable Versions]:https://gitlab.com/lunovox/components/-/tags
[Unified Inventory]:https://content.minetest.net/packages/RealBadAngel/unified_inventory/
[wiki_en_icon]:https://img.shields.io/static/v1?label=GNU%20AGPL%20v3.0&message=EN&color=blue
[wiki_en_link]:https://en.wikipedia.org/wiki/GNU_Affero_General_Public_License
[wiki_pt_icon]:https://img.shields.io/static/v1?label=GNU%20AGPL%20v3.0&message=PT&color=blue
[wiki_pt_link]:https://pt.wikipedia.org/wiki/GNU_Affero_General_Public_License
