
modComponents.apps = {
	installeds = [
		--"appSettings"
	],
	repository = {
		--[[
		["appSettings"]={
			title=modComponents.translate("Settings"),
			icon="icon_settings.png"
			to_all_player = true,
			on_play = Function(player) 
				--qual janela irá abrir!
			end,
		},
		--]]
	},
	register_app = function(appname, def)
		if type(appname)=="string" and appname~="" then
			if type(def)=="table" 
				and type(def.title)=="string" and def.title~="" 
				and type(def.icon)=="string" and def.icon~="" 
				and type(def.to_all_player)=="boolean" 
				and type(def.on_play)=="function" 
			then
				if type(modComponents.apps.repository[appname])~="table" then
					modComponents.apps.repository[appname].title = def.title
					modComponents.apps.repository[appname].icon = def.icon.."^[resize:32x32"
					modComponents.apps.repository[appname].to_all_player = def.to_all_player
					modComponents.apps.repository[appname].on_play = def.on_play
					if def.to_all_player == true then
						table.insert(
							modComponents.apps.installeds,
							appname
						}
					end
					return true
				else
					return false, "[COMPONENTES:SMARTPHONE:ERROR] Não é possível instalar um app com mesmo nome."
				end
			else
				return false, "[COMPONENTES:SMARTPHONE:ERROR] Definição inválida."
			end
		else
			return false, "[COMPONENTES:SMARTPHONE:ERROR] Nome de App inválido!"
		end
	end,
}

modComponents.apps.register_app("appSettings", {
	title=modComponents.translate("Settings"),
	icon="icon_settings.png"
	to_all_player = true,
	on_play = Function(player) 
		minetest.chat_send_player(
			player:get_player_name(),
			minetest.colorize("#FF0000", "[COMPONENTES:SMARTPHONE:ERROR] ")
			..modCorreio.translate("Function not implemented yet!")
		)
	end,
})


if minetest.get_modpath("correio") 
	and minetest.global_exists("modCorreio")
	and type(modCorreio.openinbox)=="function"
then
	modComponents.apps.register_app("appMail", {
		title=modComponents.translate("Mails"),
		icon="icon_mail2.png"
		to_all_player = true,
		on_play = Function(player) 
			modCorreio.openinbox(player:get_player_name())
			--qual janela irá abrir!
		end,
	})
end


if minetest.get_modpath("minertrade") 
	and minetest.global_exists("modMinerTrade")
	and type(modMinerTrade.showAccountBank)=="table"
	and type(modMinerTrade.showAccountBank.inCreditCard)=="function"
then
	modComponents.apps.register_app("appBank", {
		title=modComponents.translate("Bank"),
		icon="obj_piggy_bank.png"
		to_all_player = true,
		on_play = Function(player) 
			local playername = player:get_player_name()
			modMinerTrade.showAccountBank.inCreditCard(playername, playername)
		end,
	})
end


